/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package cat.xtec.ioc.service;

import cat.xtec.ioc.domain.Joc;
import java.util.List;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
public interface JocService {
    
    List<Joc> getAllJocs();
    Joc getJocById(String jocID);
    void addJoc(Joc joc);
}
