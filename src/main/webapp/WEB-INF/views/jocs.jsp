<%-- 
    Document   : jocs
    Created on : 10 may 2024, 13:05:17
    Author     : Xavi Sanz <franciscojavier.s.r.91@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Col·lecció de jocs</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">
</head>
<body>
    <div class="contenidorIdiomes">
        <a href="#" class="cat">Català</a>
        <p class="pIdiomes">|</p>
        <a href="#" class="ang">Anglès</a>
    </div>
    <div class="container">
        <h1 class="mt-5">Colecció de jocs</h1>
        <div class="col-md-6">
            <a href="<c:url value="/jocs/add" />" class="btn btn-primary btn-lg mr-2">
                <img src="<c:url value="/img/seleccionar.png" />" width="10%" height="10%">
                <span>Afegir un joc</span>
            </a>
        </div>
    </div>
    <div class="row">
    <c:forEach items="${jocs}" var="joc" varStatus="status">
        <div class="col-md-4 mb-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">${joc.name}</h5>
                    <p class="card-text">${joc.description}</p>
                    <p class="card-text">Preu: ${joc.price}</p>
                    <a href="<c:url value="/jocs/informacioJoc/${joc.jocId}" />" class="btn btn-primary btn-lg mr-2">
                        <img src="<c:url value="/img/informacio.png" />" width="10%" height="10%">
                        <span>Més informació</span>
                    </a>
                </div>
            </div>
        </div>
    </c:forEach>
</div>


</body>
</html>

