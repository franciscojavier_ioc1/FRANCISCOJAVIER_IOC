/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.service.impl;

import cat.xtec.ioc.domain.Joc;
import cat.xtec.ioc.domain.repository.JocRepository;
import cat.xtec.ioc.service.JocService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
@Service
public class JocServiceImpl implements JocService{
    
    @Autowired
    private JocRepository joc;
    /**
     * Retornem tots els jocs.
     * @return Jocs
    */
    public List<Joc> getAllJocs() {
        return joc.getAllJocs();
    }
    
    /**
     * Verifiquem si existeix el joc per al seu id, si existeix el retornem si no llança una excepció.
     * @param jocId
     * @return joc
    */
    public Joc getJocById(String jocID) {
        return joc.getJocById(jocID);
    }
    
    /**
     * Afegim joc a la llista.
     * @param joc 
    */
    public void addJoc(Joc joc) {
        this.joc.addJoc(joc);
    }
    
}
