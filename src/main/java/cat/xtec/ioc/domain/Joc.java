/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.domain;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
public class Joc {
    
    private String jocId;
    private String name; 
    private double price;
    private String description;
    private String creator;
    private String category;
    
    public Joc() {
        
    }

    public Joc(String jocId, String name, double price, String description, String creator, String category) {
        this.jocId = jocId;
        this.name = name;
        this.price = price;
        this.description = description;
        this.creator = creator;
        this.category = category;
    }

    public String getJocId() {
        return jocId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getCreator() {
        return creator;
    }

    public String getCategory() {
        return category;
    }

    public void setJocId(String jocId) {
        this.jocId = jocId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    
}
