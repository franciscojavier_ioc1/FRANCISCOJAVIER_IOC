/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package cat.xtec.ioc.domain.repository;

import cat.xtec.ioc.domain.Joc;
import java.util.List;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
public interface JocRepository {
    /**
     * Retornem tots els jocs.
     * @return Jocs
    */
    List <Joc> getAllJocs();
    
    /**
     * Verifiquem si existeix el joc per el seu id, si existeix el retornem si no llança unaexcepció.
     * @param jocId
     * @return joc
    */
    Joc getJocById(String codi);
    
    /**
     * Afegim joc a la llista.
     * @param joc 
     */
    void addJoc(Joc joc);
    
}
