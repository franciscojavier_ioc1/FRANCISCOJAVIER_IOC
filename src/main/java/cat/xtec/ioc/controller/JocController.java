/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.controller;

import cat.xtec.ioc.domain.Joc;
import cat.xtec.ioc.service.JocService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */

@Controller
@RequestMapping("/jocs")
public class JocController {
    @Autowired
    private JocService jocsService;
    
    @RequestMapping("/all")
    public ModelAndView mostrarJocs() throws ServletException, IOException {
        ModelAndView vistaModel = new ModelAndView("jocs");
        vistaModel.getModelMap().addAttribute("jocs", jocsService.getAllJocs());
        return vistaModel;
    }
    
    @RequestMapping("/informacioJoc/{jocId}")
    public ModelAndView mostrarDetallesJoc(@PathVariable("jocId") String jocId) {
        ModelAndView modelView = new ModelAndView("joc");
        Joc joc = jocsService.getJocById(jocId);
        modelView.addObject("joc", joc);
        return modelView;
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getNouJoc() throws ServletException, IOException {
        ModelAndView vistaModel = new ModelAndView("afegirJoc"); 
        Joc nouJoc = new Joc();
        vistaModel.getModelMap().addAttribute("nouJoc", nouJoc);
        return vistaModel;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String setNouJoc(@ModelAttribute("nouJoc") Joc nouJoc) throws ServletException, IOException {
        jocsService.addJoc(nouJoc);
        return "redirect:/jocs/all/";
    }
}
