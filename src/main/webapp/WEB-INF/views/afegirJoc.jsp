<%-- 
    Document   : afegirJoc
    Created on : 11 may 2024, 21:44:16
    Author     : Xavi Sanz <franciscojavier.s.r.91@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Afegir un joc</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">
    </head>
    <body>
        <div class="contenidorIdiomes">
            <a href="#" class="cat">Català</a>
            <p class="pIdiomes">|</p>
            <a href="#" class="ang"> Anglès</a>
        </div>
        <div class="container">
            <h1 class="mt-5">Afegir un joc</h1>
            <div class="col-md-6">
                <a href="<c:url value="/jocs/all" />" class="btn btn-primary btn-lg mr-2">
                    <img src="<c:url value="/img/seleccionarDarrera.png" />" width="10%" height="10%">
                    <span>Veure la colecció</span>
                </a>
            </div>
            <div class="col-md-6">
                <a href="<c:url value="/j_spring_security_logout" />" class="btn btn-primary btn-lg mr-2">
                    <img src="<c:url value="/img/seleccionarDarrera.png" />" width="10%" height="10%">
                    <span>Sortir de la sessió</span>
                </a>
            </div>
        </div>
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <h4 class="text-center">Afegir un joc</h4><br>
                    <form:form modelAttribute="nouJoc" class="form-horizontal">
                        <div class="form-group row">
                            <label for="identificador" class="col-sm-2 col-form-label">Identificador</label>
                            <div class="col-sm-10">
                                <form:input type="text" class="form-control" path="jocId" id="identificador" name="identificador" placeholder="Identificador"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nom" class="col-sm-2 col-form-label">Nom</label>
                            <div class="col-sm-10">
                                <form:input type="text" class="form-control" path="name" id="nom" name="nom" placeholder="Nom"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="preu" class="col-sm-2 col-form-label">Preu</label>
                            <div class="col-sm-10">
                                <form:input type="text" class="form-control" path="price" id="preu" name="preu" placeholder="Preu"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="creadors" class="col-sm-2 col-form-label">Creadors</label>
                            <div class="col-sm-10">
                                <form:input type="text" class="form-control" path="creator" id="creadors" name="creadors" placeholder="Creadors"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="categoria" class="col-sm-2 col-form-label">Categoria</label>
                            <div class="col-sm-10">
                                <form:input type="text" class="form-control" path="category" id="categoria" name="categoria" placeholder="Categoria"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descripcio" class="col-sm-2 col-form-label">Descripció</label>
                            <div class="col-sm-10">
                                <form:textarea class="form-control" path="description" id="descripcio" name="descripcio" rows="3" placeholder="Descripció"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 offset-sm-2">
                                <input type="submit" class="btn btn-primary btn-block" value="Crear"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </body>
</html>
