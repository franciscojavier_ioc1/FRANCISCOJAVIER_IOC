<%-- 
    Document   : joc
    Created on : 12 may 2024, 3:15:42
    Author     : Xavi Sanz <franciscojavier.s.r.91@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Joc</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">
    </head>
    <body>
         <div class="contenidorIdiomes">
        <a href="#" class="cat">Català</a>
        <p class="pIdiomes">|</p>
        <a href="#" class="ang">Anglès</a>
    </div>
    <div class="container">
        <h1 class="mt-5">Joc</h1>
        <div class="col-md-6">
            <a href="<c:url value="/jocs/all" />" class="btn btn-primary btn-lg mr-2">
                <img src="<c:url value="/img/seleccionarDarrera.png" />" width="10%" height="10%">
                <span>Veure la colecció</span>
            </a>
        </div>
    </div>
        <div class="card">
    <div class="card-body">
        <h5 class="card-title">${joc.name}</h5>
        <p class="card-text">${joc.description}</p>
        <p class="card-text"><b>Identificador:</b> ${joc.jocId}</p>
        <p class="card-text"><b>Creadors:</b> ${joc.creator}</p>
        <p class="card-text"><b>Categoria:</b> ${joc.category}</p>
        <p class="card-text"><b>Preu:</b> ${joc.price}</p>
    </div>
</div>
    </body>
</html>
