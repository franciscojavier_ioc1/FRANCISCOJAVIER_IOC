/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.domain.repository.impl;

import cat.xtec.ioc.domain.Joc;
import cat.xtec.ioc.domain.repository.JocRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
/**
 * Afegim els objectes a la llista, ja que no tenim base de dades per obtenir els jocs.
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
@Repository
public class InMemoryJocRepository implements JocRepository{
    private List<Joc> jocs = new ArrayList <Joc>();
    public InMemoryJocRepository (){
        Joc joc1 = new Joc("1","Catan",30.00,"Joc del catan", "Klaus Teuber", "Joc de taula");
        Joc joc2 = new Joc("2","Virus",15.00,"Joc del virus", "Santi Santisteban", "Joc de taula");
        Joc joc3 = new Joc("3","Dixit",35.00,"Joc del dixit", "Jean-Louis Roubira", "Joc de taula");
        Joc joc4 = new Joc("4","Màgic",19.95,"Joc del màgic", "Richard Garfield ", "Joc de cartes");
        Joc joc5 = new Joc("5","Regicide",10.00,"Joc del regicide", "Paul Abrahams, Luke Badger y Andy Richdale", "Joc de taula");
        
        jocs.add(joc1);
        jocs.add(joc2);
        jocs.add(joc3);
        jocs.add(joc4);
        jocs.add(joc5);
    }
    /**
     * Retornem tots els jocs.
     * @return Jocs
     */
    public List<Joc> getAllJocs() {
       return jocs;
    }
    
    /**
     * Verifiquem si existeix el joc per el seu id, si existeix el retornem si no llança unaexcepció.
     * @param jocId
     * @return joc
    */
    public Joc getJocById(String jocId) {
        Joc jocById = null;
        for (Joc joc : jocs) {
            if (joc != null && joc.getJocId() != null && joc.getJocId().equals(jocId)) {
                jocById = joc;
                break;
            }
        } 
        if (jocById == null) {
            throw new IllegalArgumentException("No s'han trobat jocs amb el codi: " + jocId);
        }
        return jocById;
    }  

    /**
     * Afegim un joc a la llista.
     * @param joc 
     */
    public void addJoc(Joc joc) {
        jocs.add(joc);
    }
}
