<%-- 
    Document   : jocs
    Created on : 10 may 2024, 13:05:17
    Author     : Xavi Sanz <franciscojavier.s.r.91@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Col·lecció de jocs</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">

</head>
<body>
    <div class="contenidorIdiomes">
        <a href="#" class="cat">Català</a>
        <p class="pIdiomes">|</p>
        <a href="#" class="ang"> Anglès</a>
    </div>
    <div class="container">
        <h1 class="mt-5">Benvingut/da a la col·lecció de jocs</h1>
        <p>Col·lecció de jocs de Francisco Javier Sanz</p>
        <p>He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!</p>
        <div class="col-md-6">
        <a href="<c:url value="/jocs/all" />" class="btn btn-primary btn-lg mr-2">
            <img src="<c:url value="/img/seleccionar.png" />" width="10%" height="10%">
            <span>Veure la col·lecció</span>
        </a>
        </div>
        <div class="col-md-6">
                    <a href="<c:url value="/login" />" class="btn btn-primary btn-lg mr-2">
            <img src="<c:url value="/img/autenticar-se.png" />" width="10%" height="10%">
            <span>Autenticar-se</span>
        </a>
        </div>
    </div>
</body>
</html>
