<%-- 
    Document   : login
    Created on : 11 may 2024, 15:47:51
    Author     : Xavi Sanz <franciscojavier.s.r.91@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="contenidorIdiomes">
        <a href="#" class="cat">Català</a>
        <p class="pIdiomes">|</p>
        <a href="#" class="ang"> Anglès</a>
    </div>
    <div class="container">
        <h1 class="mt-5">Autenticació</h1>
        <div class="col-md-6">
            <a href="<c:url value="/jocs/all" />" class="btn btn-primary btn-lg mr-2">
                <img src="<c:url value="/img/seleccionarDarrera.png" />" width="10%" height="10%">
                <span>Veure la colecció</span>
            </a>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <p class="text-center">Si us plau, proporcioni les seves dades</p>
                <c:if test="${not empty error}">
                    <div class="alert alert􀀀danger">
                        Credencials incorrectes
                    </div>
                </c:if>
                <form action="<c:url value="/j_spring_security_check" />" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="j_username" placeholder="Nom d'usuari">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="j_password" placeholder="Contrasenya">
                    </div>
                    <input class="btn btn-lg btn-success btn-block" type="submit" value="Connectar">
                </form>
            </div>
        </div>
    </div>
</body>
</html>